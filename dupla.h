
struct no{
  int dado;
  struct no *prox, *ant;
};
typedef struct no Lista;



Lista* criaLista(){
  return NULL;
}

Lista * insere(Lista *l, int x){
  Lista * novono = (Lista *) malloc(sizeof(Lista));
  if(l != NULL) l->ant=novono;
  novono->dado = x;
  novono->prox = l;
  novono->ant=NULL;
  return novono;
}

void imprime(Lista *l){
  Lista *aux;
  for(aux=l; aux!=NULL; aux=aux->prox){
    printf("%d",aux->dado);
    printf("\n");
  }
}


Lista* retira(Lista *l, int valor){
  Lista * aux;
  if(l == NULL){
    return 0;
  }

  if(l->dado == valor){
    if(l->prox == NULL){
      free(l);
      l = NULL;
    }else{
      l = l->prox;
      free((l)->ant);
      l->ant = NULL;
    }
  }else{
    aux = l;
    while(aux!= NULL && aux->dado != valor)
      aux = aux->prox;
    if(aux == NULL){
      return 0;
    }
    if(aux->prox == NULL){
      aux->ant->prox = NULL;
      free(aux);
    }else{
       aux->ant->prox = aux->prox;
       aux->prox->ant = aux->ant;
       free(aux);
    }
  }
}
